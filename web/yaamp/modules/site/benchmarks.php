<br>

<div class="main-left-box">
<div class="main-left-title">火池 客户端情况分享</div>
<div class="main-left-inner">

<p style="width: 700px;">火池允许用户分享 ccminer (1.7.6+) 的挖矿算力情况.</p>

<pre class="main-left-box" style='padding: 3px; font-size: .9em; background-color: #ffffee; font-family: monospace;'>
-o stratum+tcp://<?= YAAMP_STRATUM_URL ?>:&lt;PORT&gt; -a &lt;algo&gt; -u &lt;wallet_adress&gt; -p stats
</pre>

<p style="width: 700px;">
启用此选项后，层将要求每50股共享设备统计信息（最大值为4倍）.<br/>
<br/>
您可以将此矿工选项与其他选项组合起来，如<a href="/site/diff">pool difficulty</a> 用逗号隔开。<br/>
<br/>
如果没有有效地址，也可以使用通用用户名 '<b>benchmark</b>'。<br/>
但在这种情况下，你将没有收益。
</p>

<p style="width: 700px;">
请注意，在多GPU系统默认只有第一个设备属性提交。.<br/>
如果你想监控不同的卡，请使用 <b>--device</b> 参数, 如 <b>-d 1</b>
</p>

<p style="margin-bottom: 0; font-weight: bold;">你可以在以下地址下载ccminer :</p>
<ul>
<li><a href="https://github.com/tpruvot/ccminer/releases" target="_blank">https://github.com/tpruvot/ccminer/releases</a></li>
<li><a href="https://github.com/KlausT/ccminer/releases" target="_blank">https://github.com/KlausT/ccminer/releases</a></li>
</ul>

<br><br><br><br><br><br><br><br><br><br>
<br><br><br><br><br><br><br><br><br><br>
<br><br><br><br><br><br><br><br><br><br>

