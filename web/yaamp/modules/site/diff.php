<br>

<div class="main-left-box">
<div class="main-left-title">火池-自定义挖矿难度</div>
<div class="main-left-inner">

<p style="width: 700px;">默认情况下，矿池会自动调整你的矿工难度自动延续到
每分钟有5到15次提交。这是一个很好的带宽和精确度之间的折衷方案。</p>

<p style="width: 700px;">您还可以使用密码参数设置一个固定的自定义困难。例如,
//如果您想设置难度为64，请使用:</p>

<pre class="main-left-box" style='padding: 3px; font-size: .9em; background-color: #ffffee; font-family: monospace;'>
-o stratum+tcp://<?= YAAMP_STRATUM_URL ?>:3433 -u wallet_adress -p d=64
</pre>

<p style="width: 700px;">下面是自定义范围接受值:</p>

<p>Scrypt, Scrypt-N 以及 Neoscrypt:  2 - 65536</p>

<p>X11, X13, X14 以及 X15:  0.002 - 0.512</p>

<p>Lyra2:  0.01 - 2048</p>

<p style="width: 700px;">如果困难高于一枚被开采的硬币，它将被迫下降到合适。
最低硬币的困难.</p>

<br><br><br><br><br><br><br><br><br><br>
<br><br><br><br><br><br><br><br><br><br>
<br><br><br><br><br><br><br><br><br><br>



